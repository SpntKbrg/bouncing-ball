################################################
# Project Name : Breakout With Fog of War      #
# Version      : Side Prototype I Rev 2        #
# Continue From: Side Prototype I Rev 1        #
# Date (DMY)   : 09/03/2015                    #
# Author       : Quartz @ Citrine Mk. X        #
# Aim          : Colourful Screensaver         #
# New Features : Impact Traces - D             #
# Extra Work   : Transparency Fix - D          #
#                Anti Aliasing Circles - D     #
# Important Notice ** Use Pygame 1.9.0 gfxdraw #
#                     Very Buggy               #
################################################

# Functions Call
import pygame
import sys
import math
import pygame.gfxdraw
from pygame.locals import *

# System Init
FPS = 60
# Resolution To Be Flexible
WINDOWWIDTH = 1366
WINDOWHEIGHT = 768
TITLE = "Look at da colours"


def main():
    # Game Init
    global FPSCLOCK, DISPLAYSURF, GAMESURF
    pygame.init()
    FPSCLOCK = pygame.time.Clock()
    DISPLAYSURF = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
    (WINDOWWIDTH, WINDOWHEIGHT) = DISPLAYSURF.get_size()
    DEFFONT = pygame.font.Font('freesansbold.ttf', 12)
    #GAMESURF = pygame.display.set_mode((int(round(WINDOWWIDTH * 0.75)), int(round(WINDOWHEIGHT * 14 / 15))))
    pygame.display.set_caption(TITLE)
        # Game Variable Init
    start_Veloc = 5.00
    start_Accel = 0.05
    impact_Accel = 0.02
    start_Angle = 45.00
    start_PosiX = int(round(WINDOWWIDTH * 0.5))
    start_PosiY = int(round(WINDOWHEIGHT * 0.8))
    ball_Radis = 20
    max_Veloc = 15.0
    clr_Timer = 0
    ball_PosiX = start_PosiX
    ball_PosiY = start_PosiY
    ball_Veloc = start_Veloc
    ball_Accel = start_Accel
    ball_Angle = start_Angle
    #  Colour Vars
    clr_Type = 0
    bg_max = 71
    bg_min = 28
    bl_max = 154
    bl_min = 59
    vars_BGcv = bg_max
    vars_BLcv = bl_max
    vars_chag = 1
    curr_Chrom = ball_PosiX
    Impact = []
    Impact_Mxoc = 220
    Impact_cnt = 0
    Impact_Rgsi = 2

    # Game Loop
    while True:
        # Get Input
        for event in pygame.event.get():
            # Quit Signal
            if event.type == QUIT or (event.type == KEYUP and event.key == K_ESCAPE):
                pygame.quit()
                sys.exit()

        # Calculation
        ball_VeloX = int(round(ball_Veloc * math.sin(math.radians(ball_Angle))))
        ball_VeloY = int(round(ball_Veloc * math.cos(math.radians(ball_Angle))))
        ball_PosiX = int(round(ball_PosiX + ball_VeloX))
        ball_PosiY = int(round(ball_PosiY - ball_VeloY))
        ball_Veloc = ball_Veloc + ball_Accel
        if ball_PosiX <= 0 + ball_Radis and ball_Angle > 270 and ball_Angle < 360:
            ball_Veloc = ball_Veloc + impact_Accel
            ball_Angle = 360 - ball_Angle
            Impact.append([1, ball_PosiY, 0])
            Impact_cnt = Impact_cnt + 1
        elif ball_PosiX <= 0 + ball_Radis and ball_Angle > 180 and ball_Angle < 270 :
            ball_Veloc = ball_Veloc + impact_Accel
            ball_Angle = ball_Angle - 90
            Impact.append([1, ball_PosiY, 0])
            Impact_cnt = Impact_cnt + 1
        elif ball_PosiX >= WINDOWWIDTH - ball_Radis and ball_Angle < 90 and ball_Angle > 0 :
            ball_Veloc = ball_Veloc + impact_Accel
            Impact.append([WINDOWWIDTH - 1, ball_PosiY, 0])
            Impact_cnt = Impact_cnt + 1
            ball_Angle = ball_Angle + 270
        elif ball_PosiX >= WINDOWWIDTH - ball_Radis and ball_Angle < 180 and ball_Angle > 90 :
            ball_Veloc = ball_Veloc + impact_Accel
            Impact.append([WINDOWWIDTH - 1, ball_PosiY, 0])
            Impact_cnt = Impact_cnt + 1
            ball_Angle = ball_Angle + 90
        if ball_PosiY <= 0 + ball_Radis and ball_Angle < 90 and ball_Angle > 0 :
            ball_Veloc = ball_Veloc + impact_Accel
            Impact.append([ball_PosiX, 1, 0])
            Impact_cnt = Impact_cnt + 1
            ball_Angle = 90 + ball_Angle
        elif ball_PosiY >= WINDOWHEIGHT - ball_Radis and ball_Angle < 180 and ball_Angle > 90 :
            ball_Veloc = ball_Veloc + impact_Accel
            Impact.append([ball_PosiX, WINDOWHEIGHT - 1, 0])
            Impact_cnt = Impact_cnt + 1
            ball_Angle = ball_Angle - 90
        elif ball_PosiY >= WINDOWHEIGHT - ball_Radis and ball_Angle < 270 and ball_Angle > 180 :
            ball_Veloc = ball_Veloc + impact_Accel
            Impact.append([ball_PosiX, WINDOWHEIGHT - 1, 0])
            Impact_cnt = Impact_cnt + 1
            ball_Angle = ball_Angle + 90
        elif ball_PosiY <= 0 + ball_Radis and ball_Angle > 270 and ball_Angle < 360 :
            ball_Veloc = ball_Veloc + impact_Accel
            Impact.append([ball_PosiX, 1, 0])
            Impact_cnt = Impact_cnt + 1
            ball_Angle = ball_Angle - 90
        if ball_Angle <= 0 or ball_Angle == 90 or ball_Angle == 180 or ball_Angle == 270:
            ball_Angle = ball_Angle + 1
        if ball_Veloc > max_Veloc :
            ball_Veloc = max_Veloc
        if ball_Angle >= 360 :
            ball_Angle = ball_Angle - 360

            # Background Colour
        vars_BGcv = vars_BGcv + (1 * vars_chag)
        vars_BLcv = vars_BLcv + (2 * vars_chag)
        if vars_BGcv < bg_min or vars_BGcv > bg_max :
            clr_Type = clr_Type - 1
            if vars_chag == 1 :
                vars_BGcv = bg_max
                vars_BLcv = bl_max
            elif vars_chag == -1 :
                vars_BGcv = bg_min
                vars_BLcv = bl_min
            if clr_Type < 0 :
                clr_Type = 5
        if clr_Type == 0 :
            BG_CLR = (bg_max, vars_BGcv, bg_min)
            BL_CLR = (bl_max, vars_BLcv, bl_min)
            vars_chag = -1
        elif clr_Type == 1 :
            BG_CLR = (vars_BGcv, bg_max, bg_min)
            BL_CLR = (vars_BLcv, bl_max, bl_min)
            vars_chag = 1
        elif clr_Type == 2 :
            BG_CLR = (bg_min, bg_max, vars_BGcv)
            BL_CLR = (bl_min, bl_max, vars_BLcv)
            vars_chag = -1
        elif clr_Type == 3 :
            BG_CLR = (bg_min, vars_BGcv, bg_max)
            BL_CLR = (bl_min, vars_BLcv, bl_max)
            vars_chag = 1
        elif clr_Type == 4 :
            BG_CLR = (vars_BGcv, bg_min, bg_max)
            BL_CLR = (vars_BLcv, bl_min, bl_max)
            vars_chag = -1
        elif clr_Type == 5 :
            BG_CLR = (bg_max, bg_min, vars_BGcv)
            BL_CLR = (bl_max, bl_min, vars_BLcv)
            vars_chag = 1

        # Fill Background
        DISPLAYSURF.fill(BG_CLR)

        # Impact Circle Calc
        index = 0
        Impact_crdw = 0
        while index < Impact_cnt :
            Impact[index][2] = Impact[index][2] + 1
            if Impact_Mxoc - Impact[index][2] <= 0 :
                Impact.pop(index)
                Impact_cnt = Impact_cnt - 1
            else :
                Impact_crdw = Impact_crdw + 1
                pygame.gfxdraw.aacircle(DISPLAYSURF, Impact[index][0], Impact[index][1], Impact[index][2]*3, (BL_CLR[0], BL_CLR[1], BL_CLR[2], (Impact_Mxoc - Impact[index][2])))
                pygame.gfxdraw.filled_circle(DISPLAYSURF, Impact[index][0], Impact[index][1], Impact[index][2]*3, (BL_CLR[0], BL_CLR[1], BL_CLR[2], (Impact_Mxoc - Impact[index][2])))
                index = index + 1


        # Draw Gamescreen
            # Debug Menu
        DEBUGSURF = DEFFONT.render('(%s, %s) - %s - %s %s %s - %s X %s - %s + %s'% (ball_PosiX, ball_PosiY, ball_Angle, vars_BLcv, vars_BGcv, clr_Timer, WINDOWWIDTH, WINDOWHEIGHT, Impact_cnt, Impact_crdw), 1, (255, 255, 255))
        DEBUGRect = DEBUGSURF.get_rect()
        DEBUGRect.topright = (WINDOWWIDTH - 50, 10)
        DISPLAYSURF.blit(DEBUGSURF, DEBUGRect)

        #GAMESURF.fill((20, 20, 20))
        pygame.gfxdraw.aacircle(DISPLAYSURF, ball_PosiX, ball_PosiY, ball_Radis, BL_CLR)
        pygame.draw.circle(DISPLAYSURF, BL_CLR, [ball_PosiX, ball_PosiY], ball_Radis)
        #DISPLAYSURF.blit(GAMESURF, [0, 0, int(round(WINDOWWIDTH * 0.75)), int(round(WINDOWHEIGHT * 14 / 15))] )


        # Refresh Screen
        pygame.display.update()
        #pygame.display.flip()
        FPSCLOCK.tick(FPS)


if __name__ == '__main__':
    main()
