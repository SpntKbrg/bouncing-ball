################################################
# Project Name : Breakout With Fog of War      #
# Version      : Prototype I Rev 1             #
# Date (DMY)   : 09/03/2015                    #
# Author       : Quartz @ Citrine Mk. X        #
# Aim          : Calculate Ball Ricochet       #
# Extra Work   : Ball Size - D                 #
#                Game / Status Screen - W      #  
#                Impact Circle - W             #  
#                                              #  
################################################

# Functions Call
import pygame, sys, math, random
from pygame.locals import *

# System Init
FPS = 60
    # Resolution To Be Flexible
WINDOWWIDTH = 1366
WINDOWHEIGHT = 768
TITLE = "Where The Hell Are The Blocks (Placeholder Name)"
       

def main():
    # Game Init
    global FPSCLOCK, DISPLAYSURF, GAMESURF
    pygame.init()
    FPSCLOCK = pygame.time.Clock()
    DISPLAYSURF = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
    #DISPLAYSURF = pygame.FULLSCREEN
    DEFFONT = pygame.font.Font('freesansbold.ttf', 12)
    #GAMESURF = pygame.display.set_mode((int(round(WINDOWWIDTH * 0.75)), int(round(WINDOWHEIGHT * 14 / 15))))
    pygame.display.set_caption(TITLE)
        # Game Variable Init
            # Mouse Coordinate
    mouseX = 0
    mouseY = 0
    start_Veloc = 10.00
    start_Accel = 0.05
    impact_Accel = 0.02
    start_Angle = 45.00
    start_PosiX = int(round(WINDOWWIDTH * 0.5 * 0.75))
    start_PosiY = int(round(WINDOWHEIGHT * 0.8 * 14 / 15))
    ball_Radis = 10
    max_Veloc = 20.0
    ball_PosiX = start_PosiX
    ball_PosiY = start_PosiY
    ball_Veloc = start_Veloc
    ball_Accel = start_Accel
    ball_Angle = start_Angle
    
    curr_Chrom = ball_PosiX
        
    # Game Loop
    while True:
    	# Get Input
    	for event in pygame.event.get():
            # Quit Signal
            if event.type == QUIT or (event.type == KEYUP and event.key == K_ESCAPE):
            	pygame.quit()
            	sys.exit()
        
        # Calculation
        ball_VeloX = int(round(ball_Veloc * math.sin(math.radians(ball_Angle)) ))
        ball_VeloY = int(round(ball_Veloc * math.cos(math.radians(ball_Angle)) ))
        ball_PosiX = int(round(ball_PosiX + ball_VeloX))
        ball_PosiY = int(round(ball_PosiY - ball_VeloY))
        ball_Veloc = ball_Veloc + ball_Accel
        if ball_PosiX <= 0 + ball_Radis and ball_Angle > 270 and ball_Angle < 360:
            ball_Veloc = ball_Veloc + impact_Accel
            ball_Angle = 360 - ball_Angle
        elif ball_PosiX <= 0 + ball_Radis and ball_Angle > 180 and ball_Angle < 270 :
            ball_Veloc = ball_Veloc + impact_Accel
            ball_Angle = ball_Angle - 90
        elif ball_PosiX >= WINDOWWIDTH - ball_Radis and ball_Angle < 90 and ball_Angle > 0 :
            ball_Veloc = ball_Veloc + impact_Accel
            ball_PosiX = WINDOWWIDTH - ball_Radis
            ball_Angle = ball_Angle + 270
        elif ball_PosiX >= WINDOWWIDTH - ball_Radis and ball_Angle < 180 and ball_Angle > 90 :
            ball_Veloc = ball_Veloc + impact_Accel
            ball_PosiX = WINDOWWIDTH - ball_Radis
            ball_Angle = ball_Angle + 90
        if ball_PosiY <= 0 + ball_Radis and ball_Angle < 90 and ball_Angle > 0 :
            ball_Veloc = ball_Veloc + impact_Accel
            ball_Angle = 90 + ball_Angle
        elif ball_PosiY >= WINDOWHEIGHT - ball_Radis and ball_Angle < 180 and ball_Angle > 90 :
            ball_Veloc = ball_Veloc + impact_Accel
            ball_PosiY = WINDOWHEIGHT - ball_Radis
            ball_Angle = ball_Angle - 90
        elif ball_PosiY >= WINDOWHEIGHT - ball_Radis and ball_Angle < 270 and ball_Angle > 180 :
            ball_Veloc = ball_Veloc + impact_Accel
            ball_PosiY = WINDOWHEIGHT - ball_Radis
            ball_Angle = ball_Angle + 90
        elif ball_PosiY <= 0 + ball_Radis and ball_Angle > 270 and ball_Angle < 360 :
            ball_Veloc = ball_Veloc + impact_Accel
            ball_Angle = ball_Angle - 90
        if ball_Angle <= 0 or ball_Angle == 90 or ball_Angle == 180 or ball_Angle == 270:
            ball_Angle = ball_Angle + 1
        if ball_Veloc > max_Veloc :
            ball_Veloc = max_Veloc
        if ball_Angle >= 360 :
            ball_Angle = ball_Angle - 360
            
        # Draw Gamescreen
        DISPLAYSURF.fill((0, 0, 0))
        DEBUGSURF = DEFFONT.render('(%s, %s) - %s'% (ball_PosiX, ball_PosiY, ball_Angle), 1, (255, 255, 255))
    	DEBUGRect = DEBUGSURF.get_rect()
    	DEBUGRect.topright = (WINDOWWIDTH - 50, 10)
    	DISPLAYSURF.blit(DEBUGSURF, DEBUGRect)
        #GAMESURF.fill((20, 20, 20))
    	pygame.draw.circle(DISPLAYSURF, (255, 255, 255), [ball_PosiX, ball_PosiY], ball_Radis)
    	#DISPLAYSURF.blit(GAMESURF, [0, 0, int(round(WINDOWWIDTH * 0.75)), int(round(WINDOWHEIGHT * 14 / 15))] )
    	
        
        # Refresh Screen
        pygame.display.update()
        FPSCLOCK.tick(FPS)

if __name__ == '__main__':
    main()
